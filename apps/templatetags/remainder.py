# -*- coding: utf-8 -*-
# Created Time: 2016-05-10 22:52:11


from django import template

register = template.Library()
@register.filter(name="get_remainder")
def get_remainder(value, arg):
    '''''取余'''
    q = arg.split(',')
    start = q[0]
    end = int(q[1])
    if q[0] == 'start':
        if value % end == 0:
            return '<li>'
        else:
            return ''
    elif q[0] == 'end':
         if value % end == 0:
            return '</li>'
         else:
            return ''


@register.filter(name="conver_language")
def conver_language(value):
    '''''将数字转成繁体'''
    target = dict(enumerate(['壹','贰','叁','肆','伍','陆','柒','捌','玖','拾'], 1))
    if value <= 10:
        return target.get(value)
    else:
        return value



