# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# RichTextUploadingField 支持图片上传

LANGUAGE= {
    1: '中文',
    2: 'English',
}


class Images(models.Model):
    """ 网站图片设置
    底部 微信公众号 和 其他类型的图片
    """
    name = models.CharField(max_length=100, verbose_name='图片名称')
    img = models.ImageField(upload_to='img/other/',
        default='/images/img/other/default.jpg', verbose_name='展示图片')

    class Meta:
        verbose_name_plural = verbose_name = '图片配置'

    def __str__(self):
        return "%s" % self.name

class File(models.Model):
    """ 文件
    """
    name = models.CharField(max_length=100, verbose_name='文件名称')
    files = models.FileField(upload_to='file/',
        default='/images/file/default.video', verbose_name='文件')
    img = models.ImageField(upload_to='img/product/',
        default='/images/img/product/default.jpg', verbose_name='展示图片')

    class Meta:
        verbose_name_plural = verbose_name = '文件'

    def __str__(self):
        return "%s" % self.name


class NavUrls(models.Model):
    url = models.CharField(max_length=40, verbose_name='指向地址', help_text="该导航栏指向的地址")
    name = models.CharField(max_length=40, verbose_name='url 名称')

    class Meta:
        verbose_name_plural = verbose_name = '导航栏 URL配置'

    def __str__(self):
        return "%s" % self.name


class Nav(models.Model):
    name = models.CharField(max_length=40, verbose_name='导航条名称')
    introduction = models.CharField(max_length=100, null=True, blank=True, verbose_name='简介')
    priority = models.SmallIntegerField(default=1, verbose_name='排列顺序')
    url = models.ForeignKey(NavUrls, verbose_name='url')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = "导航条"

    def __str__(self):
        return "%s" % self.name


class NavImages(models.Model):
    """ 导航栏图片
    """
    nav = models.ForeignKey(Nav, verbose_name='选择导航栏')
    img = models.ImageField(upload_to='img/nav/',
        default='/static/img/nav/default.jpg', verbose_name='首页轮播图片')
    priority = models.SmallIntegerField(default=0, verbose_name='图片更换顺序',
                               help_text='数字越大, 优先级越高')

    class Meta:
        verbose_name_plural = verbose_name = '网站头部图片设置'

    def __str__(self):
        return "%s" % self.nav


class Carousel(models.Model):
    """ 首页轮播图
    """
    img = models.ImageField(upload_to='img/carousel/',
        default='/static/img/carousel/default.jpg', verbose_name='首页轮播图片')
    priority = models.SmallIntegerField(default=0, verbose_name='图片更换顺序',
                               help_text='数字越大, 优先级越高')

    class Meta:
        verbose_name_plural = verbose_name = '首页轮播图片设置'

    def __str__(self):
        return "%s" % self.img


class News(models.Model):

    NEWS = {
        0: '公司新闻',
        1: '行业新闻',
    }
    nav = models.ForeignKey(Nav, verbose_name='类别')
    title = models.CharField(max_length=100, verbose_name='标题')
    content = RichTextUploadingField(verbose_name='内容')
    types = models.IntegerField(default=0, choices=NEWS.items(),
                                    verbose_name='新闻类别')
    img = models.ForeignKey(Images, blank=True, null=True, verbose_name='首页新闻展示图片')
    pub_time = models.DateTimeField('发布时间', auto_now_add=True)
    view_times = models.IntegerField(default=1, verbose_name='阅读次数')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '资讯'
        ordering = ['-pub_time']


    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('new_view')


    def __str__(self):
        return "%s" % self.title


class Jobs(models.Model):
    name = models.CharField(max_length=100, verbose_name='招聘职位')
    department = models.CharField(max_length=100, verbose_name='招聘部门')
    degree = models.CharField(max_length=100, verbose_name='学历要求')
    num = models.SmallIntegerField(verbose_name='招聘人数', default=1)
    pub_time = models.DateTimeField(auto_now_add=True)
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '招聘信息'
        ordering = ['-pub_time']

    def __str__(self):
        return "%s" % self.name


class ProductTypes(models.Model):
    """ 产品类别
    """
    name = models.CharField(max_length=100, verbose_name='类别')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    def __str__(self):
        return "%s" % self.name


class Product(models.Model):
    types = models.ForeignKey(ProductTypes, verbose_name='类别')
    name = models.CharField(max_length=100, verbose_name='产品名称')
    version = models.CharField(max_length=100, default='', verbose_name='产品型号')
    introduce = RichTextUploadingField(verbose_name='简介')
    pub_time = models.DateTimeField(auto_now_add=True)
    img = models.ImageField(upload_to='img/product/',
        default='/images/img/product/default.jpg', verbose_name='产品图片')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '产品'

    def __str__(self):
        return "%s" % self.name


class FileDown(models.Model):
    name = models.CharField(max_length=100, verbose_name='文件名')
    update_time = models.DateTimeField(auto_now_add=True, verbose_name='更新时间')
    down_times = models.IntegerField(verbose_name='下载次数')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')
    files = models.ForeignKey(File, verbose_name='文件')

    class Meta:
        verbose_name_plural = verbose_name = '文件下载'

    def __str__(self):
        return "%s" % self.name


class LeaveMessage(models.Model):
    title = models.CharField(max_length=20, blank=True, null=True, verbose_name='留言标题')
    nickname = models.CharField(max_length=20, verbose_name='用户昵称')
    telephone = models.CharField(max_length=20, verbose_name='电话号码')
    email = models.EmailField(blank=True, null=True, verbose_name='电子邮箱')
    qq = models.CharField(max_length=13, verbose_name='在线QQ')
    time = models.DateTimeField(verbose_name='留言时间', auto_now=True)
    content = models.TextField(verbose_name='留言类容')

    class Meta:
        verbose_name_plural = verbose_name = '留言板'

    def __str__(self):
        return "%s" % self.title


class SubTitle(models.Model):
    name = models.CharField(max_length=40, verbose_name='标题名称')
    nav = models.ForeignKey(Nav, verbose_name='选择导航栏')
    introduce = models.TextField(verbose_name='简介', blank=True, null=True, help_text="首页显示的简介")
    content = RichTextUploadingField(verbose_name='内容')
    top = models.BooleanField(verbose_name='是否置顶', default=False,
        help_text='选择置顶,此文章在该专栏下,优选展示')
    img = models.ForeignKey(Images, blank=True, null=True, verbose_name='图片')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '子标题'

    def __str__(self):
        return "%s" % self.name


class Honor(models.Model):
    name = models.CharField(max_length=250, verbose_name='名称')
    img = models.ImageField(upload_to='images/honor/',
        default='/images/logs/default.jpg',verbose_name='荣誉图片')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '公司荣誉'

    def __str__(self):
        return "%s" % self.name


class SysConfig(models.Model):
    logo = models.ImageField(upload_to='images/logos/',
        default='/images/logs/default.jpg', verbose_name='网站logo')
    title = models.CharField(max_length=255, default='', verbose_name='网站名称')
    copyrights = models.CharField(max_length=255, default='', verbose_name='版权信息')
    fax = models.CharField(max_length=15, blank=True, null=True, verbose_name='企业传真')
    email = models.CharField(max_length=30, verbose_name='邮箱')
    address = models.CharField(max_length=255)
    service_phone = models.CharField(max_length=15, verbose_name='服务热线')
    telephone = models.CharField(max_length=11, verbose_name='手机号码', help_text="信息咨询手机号")
    qq = models.IntegerField(blank=True, null=True, verbose_name='QQ 号码')
    business_phone = models.CharField(max_length=15, verbose_name='招商电话')
    record = models.CharField(max_length=255, default='', verbose_name='备案号')
    public = models.ImageField(upload_to='images/wx/',
        default='/images/logs/default.jpg',verbose_name='公众号')
    subscribe = models.ImageField(upload_to='images/wx/',
        default='/images/logs/default.jpg',verbose_name='订阅号')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '网站配置'

    def __str__(self):
        return "%s" % self.title

class WxImage(models.Model):
    name = models.CharField(max_length=250, verbose_name='名称')
    img = models.ForeignKey(Images, verbose_name='图片')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')

    class Meta:
        verbose_name_plural = verbose_name = '微信图片设置'

    def __str__(self):
        return "%s" % self.name


class CompanyImage(models.Model):
    """ 企业简介
    """
    name = models.CharField(max_length=100, verbose_name='名称')
    acronym = models.CharField(max_length=100, verbose_name='英文名称')
    intro = models.TextField(verbose_name='简介')
    img = models.ImageField(verbose_name='图片',upload_to='images/company/',
        default='/images/logs/default.jpg')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')


    class Meta:
        verbose_name_plural = verbose_name = '企业简介'

    def __str__(self):
        return "%s" % self.name


class Case(models.Model):
    """ 合作案例
    """
    name = models.CharField(max_length=100, verbose_name='名称')
    url = models.URLField(blank=True, null=True, verbose_name='官网', help_text="例如:http://www.baidu.com")
    intro = models.TextField(verbose_name='简介')
    img = models.ImageField(verbose_name='图片',upload_to='images/case/',
        default='/images/logs/default.jpg')
    language_id = models.SmallIntegerField(choices=LANGUAGE.items(), verbose_name='选择语言')


    class Meta:
        verbose_name_plural = verbose_name = '合作案例'

    def __str__(self):
        return "%s" % self.name






