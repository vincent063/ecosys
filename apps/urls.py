"""ecosys URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from apps.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index_view'),
    url(r'news/$', NewsList.as_view(), name='new_view'),
    url(r'case/$', CaseList.as_view(), name='case_view'),
    url(r'common/(?P<slug>\w+)/$', CommonView.as_view(), name='common_view'),
    url(r'contact/(?P<slug>\w+)/$', ContactView.as_view(), name='contact_view'),
    url(r'news/(?P<pk>[0-9]+)/$', NewsDetailed.as_view(), name='new_details'),
    url(r'honor/$', HonorListView.as_view(), name='honor_view'),
    url(r'product/$', ProductList.as_view(), name='product_view'),
    url(r'leavemessage/$', leaveMessage, name='leave-message'),
    url(r'product/(?P<pk>[0-9]+)/$', ProductDetailed.as_view(), name="product_details")
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

