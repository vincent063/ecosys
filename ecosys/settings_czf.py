from .settings import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ecosys',
        'USER': 'root',
        'PASSWORD': 'xanadu',
        'HOST': '127.0.0.1',
        'PORT': '',
        'OPTIONS': {'charset': 'utf8mb4'},
    }
}
