# -*- coding: utf-8 -*-
# Created Time: 2016-05-02 10:05:34
from apps.logics.baseclass import ModelsProxy

class ContextMiddleware(object):
    def process_request(self, request):
        """从cookies 中获取当前language_id 设置成request 属性方便使用
        """
        # 当前语言
        language = request.COOKIES.get("language")
        if language == "English":
            language_id = 2
            show_language = "Chinese"
        else:
            language_id = 1
            show_language = "English"
        if not hasattr(request, 'language_id'):
             request.language_id = language_id

        if not hasattr(request, 'language'):
            # 显示需要设置的语言
            request.language = show_language

        if not hasattr(request, 'proxy'):
             request.proxy = ModelsProxy(language_id)
