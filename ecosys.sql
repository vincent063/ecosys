-- MySQL dump 10.13  Distrib 5.6.29, for Linux (x86_64)
--
-- Host: localhost    Database: ecosys
-- ------------------------------------------------------
-- Server version	5.6.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apps_carousel`
--

DROP TABLE IF EXISTS `apps_carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(100) NOT NULL,
  `priority` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_carousel`
--

LOCK TABLES `apps_carousel` WRITE;
/*!40000 ALTER TABLE `apps_carousel` DISABLE KEYS */;
INSERT INTO `apps_carousel` VALUES (1,'img/carousel/index-banner2.jpg',0),(2,'img/carousel/index-banner3.jpg',0),(3,'img/carousel/index-banner3_Moobv7o.jpg',0),(4,'img/carousel/index-banner4.jpg',0);
/*!40000 ALTER TABLE `apps_carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_case`
--

DROP TABLE IF EXISTS `apps_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intro` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_case`
--

LOCK TABLES `apps_case` WRITE;
/*!40000 ALTER TABLE `apps_case` DISABLE KEYS */;
INSERT INTO `apps_case` VALUES (1,'baidu公司','http://www.baidu.com','全球最大的中文搜索引擎、致力于让网民更便捷地获取信息，找到所求。百度超过千亿的中文网页数据库，可以瞬间找到相关的搜索结果。','images/case/u38328513944214400323fm58s39C718720E8EBE011B398BAC0300F024.png',1);
/*!40000 ALTER TABLE `apps_case` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_companyimage`
--

DROP TABLE IF EXISTS `apps_companyimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_companyimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intro` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_companyimage`
--

LOCK TABLES `apps_companyimage` WRITE;
/*!40000 ALTER TABLE `apps_companyimage` DISABLE KEYS */;
INSERT INTO `apps_companyimage` VALUES (1,'企业历史','COMPANY PROFILE','江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致','images/company/index-enterprise-img1.jpg',1),(2,'企业简介','COMPANY PROFILE','江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致力于润滑材料和纳米材料研发生产的高科技企业，是摩力顿高级润滑产品生产研发基地','images/company/index-enterprise-img1_Y3EpaGu.jpg',1),(3,'企业核心','COMPANY PROFILE','江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致力于润滑材料和纳米材料研发生产的高科技企业，是摩力顿高级润滑产品生产研发基地','images/company/index-enterprise-img1_mcFaxu6.jpg',1);
/*!40000 ALTER TABLE `apps_companyimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_file`
--

DROP TABLE IF EXISTS `apps_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `files` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_file`
--

LOCK TABLES `apps_file` WRITE;
/*!40000 ALTER TABLE `apps_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_filedown`
--

DROP TABLE IF EXISTS `apps_filedown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_filedown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `down_times` int(11) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `files_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_filedown_files_id_1d1ab343_fk_apps_file_id` (`files_id`),
  CONSTRAINT `apps_filedown_files_id_1d1ab343_fk_apps_file_id` FOREIGN KEY (`files_id`) REFERENCES `apps_file` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_filedown`
--

LOCK TABLES `apps_filedown` WRITE;
/*!40000 ALTER TABLE `apps_filedown` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_filedown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_honor`
--

DROP TABLE IF EXISTS `apps_honor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_honor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_honor`
--

LOCK TABLES `apps_honor` WRITE;
/*!40000 ALTER TABLE `apps_honor` DISABLE KEYS */;
INSERT INTO `apps_honor` VALUES (1,'引领行业创新发展十大功勋企业家',1,'images/honor/timg.jpeg'),(2,'江苏省民营科技企业',1,'images/honor/timg_SD22pLA.jpeg'),(3,'发明',1,'images/honor/timg_WJscZ2g.jpeg'),(4,'信息',1,'images/honor/timg_hsRYYne.jpeg'),(5,'好处',1,'images/honor/timg_6OLiyGC.jpeg'),(6,'产品',1,'images/honor/timg_VZ6z5k9.jpeg');
/*!40000 ALTER TABLE `apps_honor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_images`
--

DROP TABLE IF EXISTS `apps_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_images`
--

LOCK TABLES `apps_images` WRITE;
/*!40000 ALTER TABLE `apps_images` DISABLE KEYS */;
INSERT INTO `apps_images` VALUES (1,'新闻资讯','img/other/index-banner4.jpg'),(2,'企业文化','img/other/index-enterprise-img1.jpg'),(3,'人才理念','img/other/index-enterprise-img1_opp25K8.jpg'),(4,'董事长致辞','img/other/index-enterprise-img1_ACgQnlU.jpg'),(5,'发展历程','img/other/index-enterprise-img1_3Pwg1Cw.jpg'),(6,'公司简介','img/other/index-enterprise-img1_EcvkktW.jpg');
/*!40000 ALTER TABLE `apps_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_jobs`
--

DROP TABLE IF EXISTS `apps_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `degree` varchar(100) NOT NULL,
  `num` smallint(6) NOT NULL,
  `pub_time` datetime(6) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_jobs`
--

LOCK TABLES `apps_jobs` WRITE;
/*!40000 ALTER TABLE `apps_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_leavemessage`
--

DROP TABLE IF EXISTS `apps_leavemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_leavemessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `nickname` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `email` varchar(254) DEFAULT NULL,
  `qq` varchar(13) NOT NULL,
  `time` datetime(6) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_leavemessage`
--

LOCK TABLES `apps_leavemessage` WRITE;
/*!40000 ALTER TABLE `apps_leavemessage` DISABLE KEYS */;
INSERT INTO `apps_leavemessage` VALUES (1,'[\'xxx\']','[\'vincent\']','[\'18618263726\']','[\'123@qq.com\']','[\'916450036\']','2016-05-12 01:49:15.161494','[\'kkkkk\']'),(2,'[\'xxx\']','[\'vincent\']','[\'18618263726\']','[\'123@qq.com\']','[\'916450036\']','2016-05-12 01:54:28.576657','[\'kkkkk\']'),(3,'[\'xxx\']','[\'vincent\']','[\'18618263726\']','[\'123@qq.com\']','[\'916450036\']','2016-05-12 01:57:08.804268','[\'kkkkk\']'),(4,'[\'xxx\']','[\'vincent\']','[\'18618263726\']','[\'123@qq.com\']','[\'916450036\']','2016-05-12 01:57:16.312249','[\'kkkkk\']'),(5,'[\'xxx\']','[\'vincent\']','[\'18618263726\']','[\'123@qq.com\']','[\'916450036\']','2016-05-12 01:58:13.638259','[\'kkkkk\']'),(6,'[\'123\']','[\'Vincent\']','[\'18618263726\']','[\'lishiyong@123.com\']','[\'916450036\']','2016-05-12 02:03:12.672149','[\'kkkkkkk\']'),(7,'[\'123\']','[\'Vincent\']','[\'13801231612\']','[\'qq@123.com\']','[\'6789123\']','2016-05-12 02:09:22.358769','[\'123\']'),(8,'[\'123\']','[\'Vincent\']','[\'13801231612\']','[\'qq@123.com\']','[\'6789123\']','2016-05-12 02:09:31.916888','[\'123\']'),(9,'[\'123\']','[\'Vincent\']','[\'13801231612\']','[\'qq@123.com\']','[\'6789123\']','2016-05-12 02:10:23.486517','[\'123\']'),(10,'[\'123\']','[\'Vincent\']','[\'13801231612\']','[\'qq@123.com\']','[\'6789123\']','2016-05-12 02:54:49.073795','[\'123\']');
/*!40000 ALTER TABLE `apps_leavemessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_nav`
--

DROP TABLE IF EXISTS `apps_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `introduction` varchar(100) DEFAULT NULL,
  `priority` smallint(6) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `url_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_nav_29608e0a` (`url_id`),
  CONSTRAINT `apps_nav_url_id_d3560f96_fk_apps_navurls_id` FOREIGN KEY (`url_id`) REFERENCES `apps_navurls` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_nav`
--

LOCK TABLES `apps_nav` WRITE;
/*!40000 ALTER TABLE `apps_nav` DISABLE KEYS */;
INSERT INTO `apps_nav` VALUES (1,'首页','首页',1,1,1),(2,'实力','实力',1,1,2),(3,'新闻','新闻',1,1,3),(4,'产品','产品',1,1,4),(5,'案例','案例',1,1,5),(6,'招聘','招聘',1,1,6),(7,'联系','联系',1,1,7),(9,'index','index',1,2,1);
/*!40000 ALTER TABLE `apps_nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_navbtn`
--

DROP TABLE IF EXISTS `apps_navbtn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_navbtn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `url` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `group_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_navbtn_0e939a4f` (`group_id`),
  CONSTRAINT `apps_navbtn_group_id_00895088_fk_apps_navbtngroup_id` FOREIGN KEY (`group_id`) REFERENCES `apps_navbtngroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_navbtn`
--

LOCK TABLES `apps_navbtn` WRITE;
/*!40000 ALTER TABLE `apps_navbtn` DISABLE KEYS */;
INSERT INTO `apps_navbtn` VALUES (1,'企业简介','/common/message/?nav_id=2','img/nav/intro_v1.jpg',5,2,'<p>摩力顿--纳米科技 专注润滑 江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致力于润滑材料和纳米材料研发生产的高科技企业，是摩力顿高级润滑产品生产研...</p>'),(2,'企业文化','/common/message/?nav_id=2','img/nav/intro_v2.jpg',4,2,'<p>我们的专家团队：1、研发中心：MOLEDN-USA纳米材料研发中心（Moledn-USA Nano-materials R &amp; D Center） MOLEDN-USA纳米材料科技研发中心，是一致力于高科技纳米材料研发、应用研究、分析评定、技术服务于一体的全球性科研机构。目前中心在纳米材料 研&hellip;</p>'),(3,'董事长致辞','/common/message/?nav_id=2','img/nav/intro_v4.jpg',3,2,'<p>科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！ 风雨征程中江苏摩力顿石油化工有限公司以她的高质量和独特的纳米科研成果成为&ldquo;中国消费者满意名特优品牌&rdquo;，殊荣之下的摩力&hellip;</p>'),(4,'发展历程','/common/message/?nav_id=2','img/nav/intro_v3.jpg',2,2,'<p>我公司自创立以来，统一使用MOLEDN商标。车用润滑油又根据其原理特点不同分为：MOLEDN-M9系列、MOLEDN-M6系列、 MOLEDN-低碳系列、MOLEDN-通用四大系列。 MOLEDN-M9系列产品，以进口合成油为基础，加入进口添加剂，并特别添加了MOLEDN-M9纳米材料科技研发中&hellip;</p>');
/*!40000 ALTER TABLE `apps_navbtn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_navbtngroup`
--

DROP TABLE IF EXISTS `apps_navbtngroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_navbtngroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_navbtngroup`
--

LOCK TABLES `apps_navbtngroup` WRITE;
/*!40000 ALTER TABLE `apps_navbtngroup` DISABLE KEYS */;
INSERT INTO `apps_navbtngroup` VALUES (1,'企业简介',1),(2,'企业实力',1);
/*!40000 ALTER TABLE `apps_navbtngroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_navimages`
--

DROP TABLE IF EXISTS `apps_navimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_navimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(100) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `nav_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_navimages_nav_id_06fcd1a0_fk_apps_nav_id` (`nav_id`),
  CONSTRAINT `apps_navimages_nav_id_06fcd1a0_fk_apps_nav_id` FOREIGN KEY (`nav_id`) REFERENCES `apps_nav` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_navimages`
--

LOCK TABLES `apps_navimages` WRITE;
/*!40000 ALTER TABLE `apps_navimages` DISABLE KEYS */;
INSERT INTO `apps_navimages` VALUES (1,'img/nav/news-banner1.jpg',0,2),(2,'img/nav/news-banner1_mY5QNmS.jpg',0,3),(3,'img/nav/news-banner1_feP64ta.jpg',0,4),(4,'img/nav/news-banner1_6ulazwt.jpg',0,7),(5,'img/nav/news-banner1_9V1kn49.jpg',0,6),(6,'img/nav/timg.jpeg',0,5);
/*!40000 ALTER TABLE `apps_navimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_navurls`
--

DROP TABLE IF EXISTS `apps_navurls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_navurls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_navurls`
--

LOCK TABLES `apps_navurls` WRITE;
/*!40000 ALTER TABLE `apps_navurls` DISABLE KEYS */;
INSERT INTO `apps_navurls` VALUES (1,'/','首页'),(2,'/common/message/','实力'),(3,'/news/','新闻'),(4,'/product/','产品'),(5,'/common/case/','案例'),(6,'/common/job/','招聘'),(7,'/contact/map/','联系'),(8,'/column/service/','服务');
/*!40000 ALTER TABLE `apps_navurls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_news`
--

DROP TABLE IF EXISTS `apps_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `types` int(11) NOT NULL,
  `pub_time` datetime(6) NOT NULL,
  `view_times` int(11) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `img_id` int(11) DEFAULT NULL,
  `nav_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_news_img_id_415d1b28_fk_apps_images_id` (`img_id`),
  KEY `apps_news_nav_id_d08ab604_fk_apps_nav_id` (`nav_id`),
  CONSTRAINT `apps_news_img_id_415d1b28_fk_apps_images_id` FOREIGN KEY (`img_id`) REFERENCES `apps_images` (`id`),
  CONSTRAINT `apps_news_nav_id_d08ab604_fk_apps_nav_id` FOREIGN KEY (`nav_id`) REFERENCES `apps_nav` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_news`
--

LOCK TABLES `apps_news` WRITE;
/*!40000 ALTER TABLE `apps_news` DISABLE KEYS */;
INSERT INTO `apps_news` VALUES (1,'摩力顿润滑油2015全国巡回技术交流会朔同站成功召开','<div>\r\n<p>&nbsp;2015年5月8日，&ldquo;跨越升级&middot;润动2015，摩力顿润滑油技术交流会&rdquo;朔同站在陕西省怀仁市雲山饭店成功举办。来自朔州地区和大同地区的汽修行业、运输行业、钢铁行业的精英共同分享这场盛会，一起进行了分享、学习，共同谋划了发展之道。</p>\r\n\r\n<p><img alt=\"摩力顿-纳米技术专注润滑\" src=\"http://www.moledn.com/upload/201603/23/201603231343563378.png\" style=\"height:411px; width:548px\" title=\"摩力顿-纳米技术专注润滑\" /></p>\r\n\r\n<p>在此次的交流会上，摩力顿公司总经理闫光，讲解了行业发展趋势和摩力顿发展战略。提出服务经销商为摩力顿2015年企业营销战略落地首要工作，帮助经销从 商厂合作、公司架构管理、渠道拓展、行业大客户开发、产品系统解决方案选配、渠道售后星级服务、终端生动化陈列、员工战斗力提升做了深度且丰富的交流。</p>\r\n\r\n<p><img alt=\"摩力顿总经理讲解发展历程\" src=\"http://www.moledn.com/upload/201603/23/201603231344154877.png\" style=\"height:599px; width:551px\" title=\"摩力顿总经理讲解发展历程\" /></p>\r\n\r\n<p>此次技术交流会特别邀请了摩力顿产品顾问、中国纳米材料第一人、南京工业大学博士生导师张振忠教授给参会嘉宾从润滑油产品沿革方向阐述了过去、现在和未来 尖端科研企业的主要研究成果及研究方向。用国内外权威机构的检测数据和自己二十多年几十家服务企业经验及近百万次的实验权威的论述了活性纳米润滑油的神 奇。其产品技术已经是润滑油行业发展的第五代技术，产品的综合卓越性能在润滑油的多项指标中优于现有市面主流品牌产品，代表润滑油行业未来发展趋势。&nbsp;</p>\r\n\r\n<p><img alt=\"“摩力顿”纳米复合润滑自修复剂摩擦性能比较\" src=\"http://www.moledn.com/upload/201603/23/201603231344359174.png\" style=\"height:414px; width:552px\" title=\"“摩力顿”纳米复合润滑自修复剂摩擦性能比较\" /></p>\r\n\r\n<p>张教授几十年如一日身体力行的推动以节能减排创造绿色世界为祖国的未来留有一片蓝天为使命的伟大情怀深深的震撼了在场的每一位嘉宾及工作人员。</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.moledn.com/upload/201603/23/201603231344518731.png\" />&nbsp;&nbsp;</p>\r\n\r\n<p>摩力顿优秀经销商怀仁地区地区代理商许总即兴分享经销经验、转行经验及对润滑油行业未来发展前景的展望，乡土乡音的亲戚感与摩力顿公司合作两年从0到年销200万的心得加强了现场与会嘉宾与摩力顿公司合作的信心。</p>\r\n\r\n<p>会议举行非常圆满，现场到会意向经销全部合作，并有多家终端汽修厂也现场签约订货，活动当天总订货额高达142万，远超预期。&nbsp;&nbsp;</p>\r\n\r\n<p><img alt=\"摩力顿春季订购会\" src=\"http://www.moledn.com/upload/201603/23/201603231345109359.png\" style=\"height:411px; width:549px\" title=\"摩力顿春季订购会\" /></p>\r\n\r\n<p>此次交流会的再次成功，为摩力顿公司如何有效的帮助经销商快速做大市场，有效回笼资金找到了方法，同时更坚定了摩力顿公司帮扶经销商共成长的发展方向。摩力顿公司愿与更多致力于在润滑油行业中做大做强或掘金于未来汽车行业十年高速发展的转行经销朋友共创未来！共赢未来！</p>\r\n</div>',0,'2016-05-10 06:07:46.758722',4,1,1,3),(2,'齿轮油如何选择，齿轮油选择注意事项','<div class=\"ny_rd\">\r\n<div>在选择<strong>齿轮油</strong>时，必须要注意相关问题，避免选择存在问题和事项，可有效避免问题所在。下面，则来看下齿轮油该如何选择，齿轮油选择注意事项。<br />\r\n<br />\r\n&emsp;&emsp;1、不能用普通齿轮油代替准双曲面齿轮油。如18#双曲线齿轮油不能用于高速行驶和重载车辆的变速箱和中后桥；<br />\r\n<br />\r\n&emsp;&emsp;2、不要误认高粘度齿轮油的润滑性能好。轿车类的燃油经济性也与齿轮油粘度的选择有关，使用粘度太高标号的齿轮油，将会使燃料消耗显著增加，特别是对 高速轿车影响更大，应尽可能使用中小粘度的多级齿轮油，如75W/90、80W/90即可满足车辆变速箱及齿轮器的润滑保护,还起到更好节油效果。特别是 在北方的冬季要选用适合于当地环境温度的低温流动性更好的齿轮油（这里指的是粘度的选用），还有新车磨合期或大修磨合期粘度选用不宜过大（如选用85W- 90、80W-90、75W-90等流动性更好的齿轮油）；<br />\r\n<br />\r\n&emsp;&emsp;3、加油量应适当。油量应适当，不可过多也不可过少。过多不仅增加搅油阻力和燃料消耗，而且有可能齿轮油经后桥半轴密封圈（密封不良）漏入轮毂轴承， 造成轴承润滑脂稀释，使制动鼓制动失灵；过少会使齿轮润滑不良，温度过高，加速齿轮磨损。齿轮油面一般应加到与齿轮箱加油口下缘平齐，且应经常检查齿轮油 箱是否渗漏，并保持各油封、衬垫完好。<br />\r\n<br />\r\n&emsp;&emsp;4、适时换油。应按车辆厂家规定换油指标换用新油为最佳换油周期，如车辆经常处在苛刻的运行条件下，要缩短换油周期。换油时，应趁热放出旧油，并将齿轮和齿轮箱清洗干净后方可加入新油。加油时，应防止水分和杂质混入及其它物质混入。<br />\r\n<br />\r\n&emsp;&emsp;5、齿轮油使用禁忌。在使用中，严禁向齿轮油中加入柴油等进行稀释，不要加入增粘剂或其它抗磨剂之类的东西，也不要因影响冬季起步而烘烤后桥、变速器，以免齿轮油严重变质，出现胶质或积炭等现象。如果出现这种情况，应换用低粘度的低温流动性更好的多级齿轮油。<br />\r\n<br />\r\n&emsp;&emsp;6、在维修变速箱和后桥时用汽油或柴油零部件后必须清除干净汽油或柴油，特别是壳体内剩余的燃油，如果齿轮油中混入燃油，使用一段时间后齿轮油颜色将会变成棕红色。混入量过多，还会引起齿轮油质的变化。<br />\r\n<br />\r\n&emsp;&emsp;7、车辆齿轮油不易长期储存且不要储存在高温下。车辆齿轮油的储存：应存放在常温、凉爽干燥处。不宜在较高温度下（如高于40℃）长期储存。因为高温 会加速极压剂分解或促进添加剂之间的反应，还会影响齿轮油的泡沫性。此外，还应防止油中进水，以避免极压剂水解生成水溶性酸性物质。<br />\r\n<br />\r\n&emsp;&emsp;8、使用车辆过程中注意经常检查后桥和变速箱的通气孔是否通气不畅，变速箱温度过高等不正常现象，如出现不正常现象要及时到维修站进行全面检查。</div>\r\n\r\n<div class=\"clr\">&nbsp;</div>\r\n</div>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"newsfoot\" style=\"line-height:26px; margin-bottom:10px; margin-top:30px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>',0,'2016-05-10 07:54:58.633773',8,1,1,3),(3,'摩力顿纳米修复剂缓解机械密封磨损','<div class=\"ny_rd\">\r\n<div>工程机械在长时间运行中会出现磨损情况，密封也同样会出现磨损，有杂质出现。这些外在因素常常导致机械在运行中会出现各种故障。为了避免机械磨损故障出现，更好解决本质问题。摩力顿研发生产的<a href=\"http://www.moledn.com/products_show.aspx?id=1168\" target=\"_blank\"><strong>机械密封修复剂</strong></a>，从本质上解决机械密封磨损问题，跟进一步对机械密封进行修复。<br />\r\n<br />\r\n摩力顿机械密封修复剂以纳米修复技术为基准，经过除油泥、探伤、检查工件同心度、去除工件金属疲劳层、清洗工件、吸附修复工件等步骤达到修复要求。在常温下进行修复对工件本身不造成二次伤害，并使磨损工件修复后的光洁度、硬度、结合度和耐磨度好，不产生热应力和变形。<br />\r\n<br />\r\n机械密封件在机械运动时，会增密封各个接触面滑动摩擦，及时有润滑油进入密封中，也只是减少滑动摩擦的系数。而摩力顿机械密封修复剂，添加了纳米级别的铜，转滑动摩擦为滚动摩擦，降低密封件传动能耗，节能降耗；延长机械密封件使用寿命。</div>\r\n\r\n<div class=\"clr\">&nbsp;</div>\r\n</div>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"newsfoot\" style=\"line-height:26px; margin-bottom:10px; margin-top:30px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>',0,'2016-05-10 07:55:56.789226',2,1,1,3),(4,'工业润滑油检验方法 现在润滑油快速检验方法','<p>&emsp;检验方法？工业润滑油不用说了，大家都知道，但是对于质量检测，想必大家不知道如何检验。今天，则来看下工业润滑油检验方法，了解检验方法可购买现场进行检测。<br />\r\n<br />\r\n&emsp;&emsp;要想检查润滑油质量，一般要从外观，密度，粘度，闪点，凝点和倾点，水分等方面进行鉴别，下面我们就具体来了解一下。<br />\r\n<br />\r\n&emsp;&emsp;1、外观色度，则是看油品的颜色，一般精制程度越高，其烃的氧化物和硫化物脱除的越干净，颜色也就越浅。但是，即使精制的条件相同，不同油源和基属的 原油所生产的基础油，其颜色和透明度也可能是不相同的。对于新的成品润滑油，由于添加剂的使用，颜色作为判断基础油精制程度高低的指标已失去了它原来的意 义。<br />\r\n<br />\r\n&emsp;&emsp;2、密度：密度是润滑油最简单物理性能指标。工业润滑油密度随其组成中含碳、氧、硫的数量的增加而增大，因而在同样粘度或同样相对分子质量的情况下，含芳烃多的，含胶质和沥青质多的润滑油密度最大，含环烷烃多的居中，含烷烃多的最小。<br />\r\n<br />\r\n&emsp;&emsp;3、粘度：粘度直接反应油的摩擦力，是鉴别油品油性和流动性的一项指标。在不添加任何添加剂情况下，粘度越大，油膜强度越高，流动性越差。<br />\r\n<br />\r\n&emsp;&emsp;4、闪点：闪点是代表油品蒸发性的一项指标，油品的馏分越轻，蒸发性越大，其闪点也越低。反之，油品的馏分越重，蒸发性越小，其闪点也越高。同时，闪 点又是表示石油产品着火危险性的指标。油品的危险等级是根据闪点划分的，闪点在 45℃以下为易燃品，45℃以上为可燃品，在油品的储运过程中严禁将油品加热到它的闪点温度。在粘度相同的情况下，闪点越高越好。因此，用户在选用润滑油 时应根据使用温度和润滑油的工作条件进行选择。一般认为，闪点比使用 温度高 20～30℃，即可安全使用。<br />\r\n<br />\r\n&emsp;&emsp;5、水分，水分是工业润滑油中含水量的百分数。通常润滑油中水分的存在，会破坏润滑油形成的油膜，使润滑效果变差，加速有机酸对金属的 腐蚀作用，锈蚀设备，使油品容易产生沉渣。总之，润滑油中水分越少越好。<br />\r\n<br />\r\n&emsp;&emsp;如果是在现场鉴别润滑油质量，则要看定量，粘度，水分，机械杂质等。粘度可以使用落球粘度计，将待测油品与标准油品进行黏度比较，水分可以使用加热法,油品加热有明显爆裂声响则表明含水，机械杂质可以使用滤纸油渍实验，水溶性酸碱可以使用指示剂法等等。</p>',1,'2016-05-12 13:48:28.000604',2,1,1,3),(5,'润滑油添加剂有什么作用','<p>润滑油添加剂有什么作用呢？很多朋友只知道添加润滑油，却不知道润滑油添加剂有什么作用。下面则来看下润滑油添加剂有什么作用。<br />\r\n<br />\r\n&emsp;&emsp;1、清净分散剂的作用有增溶作用、分散作用、酸中和作用、吸附作用四种。<br />\r\n<br />\r\n&emsp;&emsp;2、抗氧和抗氧抗腐添加剂的作用是抑制油品的氧化过程，钝化金属对氧化的催化作用，达到延长油品使用和保护机器的目的。<br />\r\n<br />\r\n&emsp;&emsp;3、降凝剂的作用是降低油品的凝点，使油品在低温时保持良好的流动性，提高发动机的低温起动性能。<br />\r\n<br />\r\n&emsp;&emsp;4、粘度指数改进剂可以增加油品的粘度，特别是能满足油品的低温使用性能要求。<br />\r\n<br />\r\n&emsp;&emsp;5、油性剂和极压抗磨剂能与金属表面起化学反应生成化学反应膜，防止金属表面的磨损、擦伤和熔焊。<br />\r\n<br />\r\n&emsp;&emsp;6、防锈剂的作用包括在金属表面形成吸附性保护层、防止腐蚀介质与金属接触、起到防锈作用。<br />\r\n<br />\r\n&emsp;&emsp;7、抗泡剂的主要作用是抑制泡沫的产生，以免形成安定的泡沫，它能吸附在泡膜上，形成不安定的膜，从而达到破坏泡沫的目的。<br />\r\n<br />\r\n&emsp;&emsp;8、抗乳化剂能改变油/水界面的张力，使油水分离，达到改善油品的抗乳化性能的目的。</p>',1,'2016-05-12 13:49:02.716710',3,1,NULL,3),(6,'工业润滑油检验方法 现在润滑油快速检验方法','<p>&emsp;&emsp;检验方法？工业润滑油不用说了，大家都知道，但是对于质量检测，想必大家不知道如何检验。今天，则来看下工业润滑油检验方法，了解检验方法可购买现场进行检测。<br />\r\n<br />\r\n&emsp;&emsp;要想检查润滑油质量，一般要从外观，密度，粘度，闪点，凝点和倾点，水分等方面进行鉴别，下面我们就具体来了解一下。<br />\r\n<br />\r\n&emsp;&emsp;1、外观色度，则是看油品的颜色，一般精制程度越高，其烃的氧化物和硫化物脱除的越干净，颜色也就越浅。但是，即使精制的条件相同，不同油源和基属的 原油所生产的基础油，其颜色和透明度也可能是不相同的。对于新的成品润滑油，由于添加剂的使用，颜色作为判断基础油精制程度高低的指标已失去了它原来的意 义。<br />\r\n<br />\r\n&emsp;&emsp;2、密度：密度是润滑油最简单物理性能指标。工业润滑油密度随其组成中含碳、氧、硫的数量的增加而增大，因而在同样粘度或同样相对分子质量的情况下，含芳烃多的，含胶质和沥青质多的润滑油密度最大，含环烷烃多的居中，含烷烃多的最小。<br />\r\n<br />\r\n&emsp;&emsp;3、粘度：粘度直接反应油的摩擦力，是鉴别油品油性和流动性的一项指标。在不添加任何添加剂情况下，粘度越大，油膜强度越高，流动性越差。<br />\r\n<br />\r\n&emsp;&emsp;4、闪点：闪点是代表油品蒸发性的一项指标，油品的馏分越轻，蒸发性越大，其闪点也越低。反之，油品的馏分越重，蒸发性越小，其闪点也越高。同时，闪 点又是表示石油产品着火危险性的指标。油品的危险等级是根据闪点划分的，闪点在 45℃以下为易燃品，45℃以上为可燃品，在油品的储运过程中严禁将油品加热到它的闪点温度。在粘度相同的情况下，闪点越高越好。因此，用户在选用润滑油 时应根据使用温度和润滑油的工作条件进行选择。一般认为，闪点比使用 温度高 20～30℃，即可安全使用。<br />\r\n<br />\r\n&emsp;&emsp;5、水分，水分是工业润滑油中含水量的百分数。通常润滑油中水分的存在，会破坏润滑油形成的油膜，使润滑效果变差，加速有机酸对金属的 腐蚀作用，锈蚀设备，使油品容易产生沉渣。总之，润滑油中水分越少越好。<br />\r\n<br />\r\n&emsp;&emsp;如果是在现场鉴别润滑油质量，则要看定量，粘度，水分，机械杂质等。粘度可以使用落球粘度计，将待测油品与标准油品进行黏度比较，水分可以使用加热法,油品加热有明显爆裂声响则表明含水，机械杂质可以使用滤纸油渍实验，水溶性酸碱可以使用指示剂法等等。</p>',1,'2016-05-12 13:49:35.754256',6,1,NULL,3),(7,'纳米润滑油如何使用 纳米润滑油添加方法','<div class=\"ny_rd\">\r\n<div>&emsp;是在原子、分子尺度研究相对转动界面上的摩擦磨损与润滑行为，并根据微观摩擦磨损机理，设计与制备出纳米尺度上的润滑剂及摩耐磨材料的学科。<br />\r\n<br />\r\n&emsp;&emsp;由于纳米润滑油粒子粒度小，表面性能高，粒子之间容易发生团聚，而纳米材料在中 的分散和稳定成为限制其在润滑油添加剂中应用的主要问题之一。因此，纳米润滑油具有良好的分散性，稳定性，表面活性剂解吸后在油中要有良好的摩擦学性能。 纳米润滑油便是根据汽油高转速的工作特性及引擎的急提速性，进而调制配方添加剂，能够符合各种类型汽柴油引擎各种情况的抗磨需求。并且能够增加油膜厚度、 增加缸压及油膜的附着性。有效的填补引擎因为内部金属的刮伤，有效的将引擎机油抗磨的有效期延长。<br />\r\n<br />\r\n&emsp;&emsp;<strong>纳米润滑油的功能效果</strong><br />\r\n<br />\r\n&emsp;&emsp;1、能够有效的修复汽缸内部被磨耗的部位，并在其表面形成纳米润滑层。<br />\r\n<br />\r\n&emsp;&emsp;2、能够有效的防止压缩压力泄露，具有非常好的密封作用。<br />\r\n<br />\r\n&emsp;&emsp;3、能够防止不完全燃烧物的产生，防止润滑油流入燃烧区内。<br />\r\n<br />\r\n&emsp;&emsp;<strong>纳米润滑油如何使用：</strong><br />\r\n<br />\r\n&emsp;&emsp;纳米润滑油与普通润滑油是不一样的，在交换润滑油注入时，表现效果最好。在以后的每200000公里进行更换，可以有效的维持其良好的效果。在对待拥 有比较严重的磨损程度的汽车发动机车，在刚开始的时候每5000公里进行一次纳米润滑油的注入，在进行了两次注入以后便可以正常注入了。注意这是说的 200000公里并不是机油的更换，机油的更换是100000公里一次。一般纳米润滑油在针对行驶5000KM以上的汽车有较好的表现，使用期限越长的车 在注入纳米润滑油后会发现其效果更加明显。一般汽油/LPG汽车的纳米润滑油注入标准为机油4升时注入500cc，柴油汽车纳米润滑油注入标准为机油时6 升500cc。</div>\r\n\r\n<div class=\"clr\">&nbsp;</div>\r\n</div>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"newsfoot\" style=\"line-height:26px; margin-bottom:10px; margin-top:30px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>',1,'2016-05-12 13:50:45.282804',7,1,NULL,3);
/*!40000 ALTER TABLE `apps_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_product`
--

DROP TABLE IF EXISTS `apps_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `introduce` longtext NOT NULL,
  `img` varchar(100) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `types_id` int(11) NOT NULL,
  `version` varchar(100) NOT NULL,
  `pub_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_product_5f546deb` (`types_id`),
  CONSTRAINT `apps_product_types_id_e7ce4d2f_fk_apps_producttypes_id` FOREIGN KEY (`types_id`) REFERENCES `apps_producttypes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_product`
--

LOCK TABLES `apps_product` WRITE;
/*!40000 ALTER TABLE `apps_product` DISABLE KEYS */;
INSERT INTO `apps_product` VALUES (1,'M6抗磨液压油 L-HM 4L','<p><strong><span style=\"color:#337FE5\">M6活性抗磨液压油</span></strong><br />\r\n<strong><span style=\"color:#337FE5\">L-HM &nbsp;46# &nbsp;68#</span></strong><br />\r\n<strong><span style=\"color:#337FE5\">抗磨减损 &nbsp;延长寿命</span></strong><br />\r\nM6活性抗磨液压油，采用深度精制的高粘度指数矿物基础油添加多功能的复合添加剂，运用Moledn-USA纳米材料研发中心的领先技术调合而成。本品除具有优良的抗磨、抗氧 &nbsp; 化、抗乳化、抗泡、防锈等性能，还具有优异的粘温特性和低温使用性能。<br />\r\n<strong><span style=\"color:#337FE5\">功能特点：</span></strong><br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;优异的抗磨性能，能够有效减缓设备磨损，延长设备使用寿命；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;良好的粘温特性和低温使用性能，保证液压元件在各种复杂温度及工作条件下得到良好的润滑、密封合冷却；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;极好的氧化安定性，减缓油品的氧化变质，极大的延长换油周期，并有效保证液压系统的清洁；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;优良的抗泡性和空气释放性，避免产生气穴和气蚀；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;优良的抗乳化性和油品分离特性，保证系统正常润滑；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;与各种密封材料均有良好的适应性。<br />\r\n<strong><span style=\"color:#337FE5\">适用范围：</span></strong><br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;适合各种车辆的液压系统。</p>','img/product/index-product-img1.jpg',1,1,'','2016-05-18 07:37:27.608375'),(2,'M6活性摩托车机油 900ml','<p><strong><span style=\"color:#337FE5\">M6活性摩托车机油</span></strong><br />\r\n<strong><span style=\"color:#337FE5\">API &nbsp;SJ &nbsp;</span></strong><br />\r\n节省燃油，增强动力&nbsp;<br />\r\n本品专为摩托车引擎而配置，运用Moledn-USA纳米材料研发中心的领先技术，采用进口合成基础油、进口复合添加剂和M9活性纳米添加剂等原料精制而成。<br />\r\n<strong><span style=\"color:#337FE5\">功能特点：</span></strong><br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;自修复：国际领先的自修复技术，先修复再养护，持久，节能；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;抗磨、减摩：降低摩擦系数60％，减少磨损90％；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;节油：平均降低油耗6-32％，延长润滑油换油周期1-3倍；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;动力：增强10-30％；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;环保：有害气体总排放量减少40～50％；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;寿命：延长发动机使用寿命1倍以上；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;降噪：降低噪音3-5dB；<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;清理积碳，分散油泥。</p>','img/product/index-product-img1_d1awEvJ.jpg',1,2,'','2016-05-18 07:37:27.608375'),(3,'bbbb','<p>bbbbb</p>','img/product/product-list-img1.jpg',1,1,'','2016-05-18 07:37:27.608375'),(4,'xxxxx','<p>dddddd</p>','img/product/index-product-img1_Do7A62j.jpg',1,1,'','2016-05-18 07:37:27.608375'),(5,'mmmm','<p>mmmmm</p>','img/product/product-list-img1_48WSoR2.jpg',1,1,'','2016-05-18 07:37:27.608375');
/*!40000 ALTER TABLE `apps_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_producttypes`
--

DROP TABLE IF EXISTS `apps_producttypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_producttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_producttypes`
--

LOCK TABLES `apps_producttypes` WRITE;
/*!40000 ALTER TABLE `apps_producttypes` DISABLE KEYS */;
INSERT INTO `apps_producttypes` VALUES (1,'车辆润滑油',1),(2,'摩托车机油',1);
/*!40000 ALTER TABLE `apps_producttypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_subtitle`
--

DROP TABLE IF EXISTS `apps_subtitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_subtitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `content` longtext NOT NULL,
  `top` tinyint(1) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `nav_id` int(11) NOT NULL,
  `img_id` int(11) DEFAULT NULL,
  `introduce` longtext,
  PRIMARY KEY (`id`),
  KEY `apps_subtitle_nav_id_3f106c5e_fk_apps_nav_id` (`nav_id`),
  KEY `apps_subtitle_05b38f98` (`img_id`),
  CONSTRAINT `apps_subtitle_img_id_6455ddb2_fk_apps_images_id` FOREIGN KEY (`img_id`) REFERENCES `apps_images` (`id`),
  CONSTRAINT `apps_subtitle_nav_id_3f106c5e_fk_apps_nav_id` FOREIGN KEY (`nav_id`) REFERENCES `apps_nav` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_subtitle`
--

LOCK TABLES `apps_subtitle` WRITE;
/*!40000 ALTER TABLE `apps_subtitle` DISABLE KEYS */;
INSERT INTO `apps_subtitle` VALUES (1,'公司简介','<div class=\"ny_rd\">\r\n<p><span style=\"color:#337fe5; font-size:16px\"><strong>摩力顿--纳米科技 专注润滑</strong></span><br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致力于润滑材料和纳米材料研发生产的高科技企业，是摩力顿高级润滑产品生产 研发基地。摩力顿公司是世界石油化工领域高端产品研发生产的实体型高科技领军企业，不断以尖端科技引领全球方向。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 公司生产基地位于江苏（徐州）邳州市明珠工业区168号。公司科研、管理人员占职工总人数的40%，其中博士6人，硕士15人，大专以上学历85人。同时 与南京工业大学、广东工业大学、中国石油科学研究院及国家石油产品质量监督检验中心等科研机构合作研发，利用MOLEDN纳米研发中心的纳米润滑技术，形 成了国内一流的专业纳米润滑材料研发、制造基地。<br />\r\n公司简称：摩力顿石化、摩力顿石油、江苏摩力顿、摩力顿纳米、摩力顿科技。江苏摩力顿石油化工有限公司生产基地占地面积50000㎡，主厂区拥有5000 吨级的基础油储备区，铁路专用线拥有2万吨级燃料油基础油储备，连云港港储备区为5万吨级储备量。同时公司拥有国际领先的纳米材料研发中心、润滑油生产车 间、纳米添加剂生产车间、燃料油调合设备以及相应的配套设施，年生产润滑油10万吨；纳米复合自修复添加剂和燃油添加剂1000吨；润滑脂1万吨；船舶燃 料油30万吨。拥有国际上最先进的生产装置和工艺技术，完善的检测、检验手段，先进的润滑油燃料油分析、测试中心，完整的科技研发体系。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 公司以领先的国际标准和美国纳米润滑技术，依托MOLEDN-USA纳米材料研发中心强大的技术研发实力，在当今纳米材料和润滑油领域拥有最先进的润滑技 术。摩力顿秉承&ldquo;科技、节能、专业&rdquo;的理念，研发出具有国际领先水平的M9和M6系列活性润滑产品，已经获得了多项国家发明专利，是首批通过 ISO9001:2008质量管理体系认证和ISO14001:2004环境管理体系认证的企业，成为&ldquo;活性润滑油&rdquo;的创领者。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 公司产品涵盖四大系列即纳米M9系列、纳米M6系列、低碳系列、通用系列。包括：车用油、工程机械用油、船用油、工业用油、润滑脂、防冻液、内燃机燃油添加剂、工业用燃油添加剂、纳米复合润滑自修复剂等系列产品。&nbsp;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 近年来，摩力顿公司在中国市场不断创新生产管理经验、优化市场营销模式、提升科技研发水平，使公司在激烈的市场竞争中始终生机勃发，企业综合实力不断提 高，生产销售，渠道物流，售后服务在行业内始终位居于行业前列。在车辆用油服务方面，MOLEDN技术服务团队拥有一支经过专业化的产品理论和汽车润滑养 护技术培训的服务队伍，专业汽车润滑系统检查；专业用油指导；专业发动机保养和配套服务；专业更换机油服务，专业跟踪指导，服务于汽车换油终端，为爱车润 滑养护提供一体化的服务。并在行业内首家采用一体化技术服务即5S技术服务，5S技术服务是摩力顿与MOLEDN-USA纳米材料科技研发中心在中国大陆 润滑油行业内率先推出的技术服务模式。</p>\r\n\r\n<p><strong>&ldquo;5S&rdquo;服务具体体现：</strong><br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1、专业润滑系统检查；&mdash; 即发动机工况检测，调整发动机工况突破在各种严苛的特殊环境下，保持动力强劲。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2、专业用油指导；&mdash; 即售前技术辅导，量身定制有针对性科学解决方案，调整油品使用常识。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3、专业设备保养和配套服务；&mdash; 即售中服务讲解，提供科学的方案，清扫润滑系统状况。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4、专业更换润滑油服务；&mdash; 即用油理念传播，一对一的贴身服务，保持润滑系统清洁。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5、专业跟踪指导。&mdash; 即售后服务支持，提倡养护比维修更重要，保持良好用油习惯，轻松应对各种路况。</p>\r\n</div>',1,1,2,6,'江苏摩力顿石油化工有限公司是2005年江苏邳州市人民政府重点引进扶持的一家致力于润滑材料和纳米材料研发生产的高科技企业，是摩力顿高级润滑产品生产 研发基地。摩力顿公司是世界石油化工领域高端产品研发生产的实体型高科技领军企业，不断以尖端科技引领全球方向。'),(2,'发展历程','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 我公司自创立以来，统一使用MOLEDN商标。车用润滑油又根据其原理特点不同分为：MOLEDN-M9系列、MOLEDN-M6系列、MOLEDN-低碳系列、MOLEDN-通用四大系列。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;MOLEDN-M9系列产品，以进口合成油为基础，加入进口添加剂，并特别添加了MOLEDN-M9纳米材料科技研发中心发明的纳米活性高科技润滑材料 配伍性极强，赋予了摩力顿润滑油的9大功效。即：修复磨损、抗磨减摩、节能减排、增强动力、降低噪音、降低温度、清理积碳、分散油泥、抗氧防腐。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 自动形成纳米轴承结构膜，使滑动摩擦变为滚动摩擦，大幅度降低摩擦磨损，有效控制发动机冷启动的破坏性磨损，延长发动机使用寿命，节能环保。自动修复，国 际领先的自修复技术，先修复再养护，持久，节能。抗磨、减摩：降低摩擦系数60％，减少磨损90％，节油：平均降低油耗6-32％，延长润滑油换油周期 1-3倍，增强动力10-30％，降低发动机有害气体总排放量40～50％，延长发动机使用寿命1-2倍以上，降低发动机噪音3-5分贝，具有清理发动机 积碳与分散油泥的显著功效。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN-M6系列以加氢精制基础油及合成油为基础，加入进口复合剂，并添加了活性高科技润滑材料配伍性极强，经特殊工艺取得了液相均化的重大突破, 解决了团聚,沉淀的技术难题,是应用纳米材料成功的范例,突出特性为超级润滑,减摩增速,自动填充修复,稳定成膜,有效控制发动机冷启动破坏性磨损,延长 发动机的使用寿命。 M6系列产品具有6大功能：抗磨减摩、节能减排、增强动力、降低噪音、清理积碳、抗氧防腐。1.降低摩擦系数35％，减少磨损50％；2.平均降低油耗 3-15％；3.增强动力10％；4、降低发动机尾气排放总排量15％；5、发动机降低噪音1-2分贝；6、清理发动机积碳。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN低碳 润滑油系列产品以加氢精制基础油为基础，加入进口添加剂，采用DTFK1158、DTFY1198、DTZN1165低碳润滑材料配伍性优良， 在主剂量ZDDP适当减少的情况下，解决了润滑油品抗氧化及减少积碳结焦效能，以低碳技术DTFK1158、DTFY1198、DTZN1165为核心的 低碳润滑油技术代表着未来润滑油的发展方向。经特殊工艺调配取得了润滑油调合的重大突破,其特点是有效降低油品中的磷含量，大幅度延长三元催化剂的使用寿 命，在先进的脉冲调合工艺保障下，独特的减磨节能剂以纳米级分散在基础油中，使摩擦系数降至最低。创新的DTFK1158、DTFY1198、 DTZN1165复配技术有效阻断油泥和积碳的生成，使换油期不断延长，同时加强了能源的使用率，更环保节能，为润滑油品达到3万公里不换油提供了有利技 术支持。极大的延长发动机的使用寿命。使低碳理念得以淋漓尽致的发挥。该产品已通过国家石油产品质量监督检测中心的性能检测。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 摩力顿通用系列产品以优质基础油为基础，加入优质添加剂，经高剪切均化、高密度过滤及特殊工艺精心调配而成。该配方中，基础油与添加剂配伍最佳，在主剂处于经济配方剂量时，抗氧化性十分突出。</p>',0,1,2,5,'我公司自创立以来，统一使用MOLEDN商标。车用润滑油又根据其原理特点不同分为：MOLEDN-M9系列、MOLEDN-M6系列、MOLEDN-低碳系列、MOLEDN-通用四大系列。'),(3,'董事长致辞','<p>科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 风雨征程中江苏摩力顿石油化工有限公司以她的高质量和独特的纳米科研成果成为&ldquo;中国消费者满意名特优品牌&rdquo;，殊荣之下的摩力顿并没有忘记她的社会责任，在 抓住机遇，加速发展的同时，以&ldquo;绿色、节能、低碳、环保&rdquo;为服务宗旨；本着服务社会，回报社会的精神，对社会环保不遗余力&hellip;&hellip;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 多年来，在复杂多变的经营形势下，本公司抓住中国经济平稳较快增长的机遇，积极扩大资源，拓展市场，优化运行，加强管理，各项业务取得良好进展，经营业绩 再创新高。公司不断完善营销网络，不断拓展业务区域，经营规模进一步扩大。国际贸易资源保障能力进一步增强。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 非常高兴地欢迎马希福教授、张振忠博士、Arco Kostov（阿科斯托夫）教授、赵芳霞博士、潘传艺博士、王伟高级工程师加入到本科研中心，他们是境内知名的节能减排专家，为本公司增添了新的活力。 2010年，本公司围绕主业加快推进科技创新。不断完善科技体制和机制，充分调动科研人员积极性和创造性，科技创新取得一批重要成果并荣获《中国质量万里 行》《质量管理体系认证》《中国消费者满意名特优品牌》《国家质量监督合格红榜产品》等一系列荣誉证书。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 近年来，本公司切实履行社会责任，塑造了良好的公司形象。根据公司发展实际，郑重提出&ldquo;纳米科技，专注润滑&rdquo;的社会责任口号，从持续保障能源、提供优质服务、安全绿色运营、践行低碳发展、悉心关爱员工、竭诚回馈社会等方面努力践行。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 本人及董事会成员相信，在各位股东和社会各方的支持下，在董事会、监事会、管理层和全体员工的共同努力下，本公司各项事业一定能够取得新的进步，为股东创造新的价值，为构建和谐社会做出新的贡献。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 董事长：吴其增</p>\r\n\r\n<p>&nbsp;<img alt=\"\" src=\"http://www.moledn.com/upload/201604/05/201604051142430313.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>',0,1,2,4,'科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！科技拉近了我们的距离，亲爱的朋友，不管您现在身处何方，我都真诚地感谢您登陆本企业的网站，和我们一起分享成长和收获的快乐！'),(4,'企业文化','<p>我们的专家团队：<br />\r\n1、研发中心：MOLEDN-USA纳米材料研发中心<br />\r\n（Moledn-USA Nano-materials R &amp; D Center）<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN-USA纳米材料科技研发中心，是一致力于高科技纳米材料研发、应用研究、分析评定、技术服务于一体的全球性科研机构。目<br />\r\n前中心在纳米材料研究及应用方面的技术研发已拥有多项世界领先的发明专利，并将研究成果充分的应用于润滑材料中，开创&ldquo;活性润滑&rdquo;技术，成为世界新型润滑材料研发的方向标。<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;研发中心下设纳米粉体材料、活性润滑油两个研究室，低碳润滑添加剂技术研发中心。承担相关专业领域产品的应用研究、产品开发、技术服务等工作。<br />\r\n2、专家团队<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;MOLEDN-USA纳米材料研发中心&mdash;&mdash;中国专家团<br />\r\n马希福教授<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN-USA纳米材料科技研发中心首席润滑油专家，中国石油化工总公司长城高级润滑油公司总工程师，我国润滑领城著名专家。他先后参与中国航天事 业--神舟系列润滑项目研发；主持研发中国首创的流化床制备全氟碳油生产工艺和氟气压缩工艺成功用于工业化生产，同时承担民用润滑油的产品开发、应用研 究、技术管理、质量管理和技术服务等，是中国润滑油品奠基人之一，并享受国务院专家特殊津贴待遇。<br />\r\n张振忠博士<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN-USA纳米材料科技研发中心首席纳米材料专家，摩力顿石油化工有限公司技术总工程师，南京工业大学材料科学与工程学院博士后、博士生导师，国内著名微纳米金属材料制备及应用学科带头人。<br />\r\nArco Kostov（阿科斯托夫）教授<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 国际著名化学家，MOLEDN-USA纳米材料科技研发中心专家，纳米科技领域的开创者之一。主要研究纳米材料在现在生活中的各种应用，已经攻克了多项世界级的难题，特别是将纳米材料应用到润滑材料中，已经获得了多项美国技术专利。<br />\r\n赵芳霞博士<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MOLEDN-USA纳米材料科技研发中心首席纳米材料专家，摩力顿石油化工有限公司技术总负责人，著名的纳米材料研究与应用的专家。专业致力于纳米复合 粉体材料制备与应用、块体金属材料与非晶体材料制备及应用、材料表面电化学沉积及废液处理、金属材料的组织与性能的研究。<br />\r\n潘传艺博士<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;MOLEDN-USA纳米材料研发中心专家，摩力顿石油化工有限公司高级技术顾问，广东工业大学博士生导师，金属加工润滑技术的应用与管理学科带头人。在金属加工液、工业润滑油方面拥有丰富的技术和经验。</p>',0,1,2,2,'研发中心下设纳米粉体材料、活性润滑油两个研究室，低碳润滑添加剂技术研发中心。承担相关专业领域产品的应用研究、产品开发、技术服务等工作。'),(5,'人才理念','<p><span style=\"font-size:16px\"><strong>摩力顿人才理念</strong></span></p>\r\n\r\n<p>1.人才是企业的资本，人才是最宝贵的财富，摩力顿的大门对所有有志于润滑油事业的人敞开着。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>2.<strong>人才理念：</strong>人力资源是企业生存发展第一资源！德才兼备为第一原则！</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>3.追求员工个人价值与企业价值的高度和谐统一！</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>4.<strong>公司的用人标准是：</strong>德才兼备为第一原则。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>5.<strong>公司竭尽所能做到：</strong>人适其位，位适其人。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>6.公司不仅通过待遇留人，公司更愿通过事业留人，感情留人。</p>',0,1,6,3,'人才是企业的资本，人才是最宝贵的财富，摩力顿的大门对所有有志于润滑油事业的人敞开着。');
/*!40000 ALTER TABLE `apps_subtitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_sysconfig`
--

DROP TABLE IF EXISTS `apps_sysconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_sysconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `copyrights` varchar(255) NOT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `service_phone` varchar(15) NOT NULL,
  `business_phone` varchar(15) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `public` varchar(100) NOT NULL,
  `record` varchar(255) NOT NULL,
  `subscribe` varchar(100) NOT NULL,
  `qq` int(11),
  `telephone` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_sysconfig`
--

LOCK TABLES `apps_sysconfig` WRITE;
/*!40000 ALTER TABLE `apps_sysconfig` DISABLE KEYS */;
INSERT INTO `apps_sysconfig` VALUES (1,'images/logos/logo_YB6twmX.png','国金通大','江苏摩力顿石油化工有限公司 苏ICP备14013878号','0516-86585299','USA@moledn.com','徐州复兴南路237号嘉利公司三楼','0516-80310665','0516-80310665',1,'images/wx/qr-code.jpg','苏ICP备14013878号','images/wx/qr-code_ut0nK8I.jpg',760739267,'18618263452');
/*!40000 ALTER TABLE `apps_sysconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_wximage`
--

DROP TABLE IF EXISTS `apps_wximage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_wximage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `img_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_wximage_img_id_ef0a65b5_fk_apps_images_id` (`img_id`),
  CONSTRAINT `apps_wximage_img_id_ef0a65b5_fk_apps_images_id` FOREIGN KEY (`img_id`) REFERENCES `apps_images` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_wximage`
--

LOCK TABLES `apps_wximage` WRITE;
/*!40000 ALTER TABLE `apps_wximage` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_wximage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add 图片配置',7,'add_images'),(20,'Can change 图片配置',7,'change_images'),(21,'Can delete 图片配置',7,'delete_images'),(22,'Can add 文件',8,'add_file'),(23,'Can change 文件',8,'change_file'),(24,'Can delete 文件',8,'delete_file'),(25,'Can add 导航栏 URL配置',9,'add_navurls'),(26,'Can change 导航栏 URL配置',9,'change_navurls'),(27,'Can delete 导航栏 URL配置',9,'delete_navurls'),(28,'Can add 导航条',10,'add_nav'),(29,'Can change 导航条',10,'change_nav'),(30,'Can delete 导航条',10,'delete_nav'),(31,'Can add 网站头部图片设置',11,'add_navimages'),(32,'Can change 网站头部图片设置',11,'change_navimages'),(33,'Can delete 网站头部图片设置',11,'delete_navimages'),(34,'Can add 首页轮播图片设置',12,'add_carousel'),(35,'Can change 首页轮播图片设置',12,'change_carousel'),(36,'Can delete 首页轮播图片设置',12,'delete_carousel'),(37,'Can add 资讯',13,'add_news'),(38,'Can change 资讯',13,'change_news'),(39,'Can delete 资讯',13,'delete_news'),(40,'Can add 招聘信息',14,'add_jobs'),(41,'Can change 招聘信息',14,'change_jobs'),(42,'Can delete 招聘信息',14,'delete_jobs'),(43,'Can add product types',15,'add_producttypes'),(44,'Can change product types',15,'change_producttypes'),(45,'Can delete product types',15,'delete_producttypes'),(46,'Can add 产品',16,'add_product'),(47,'Can change 产品',16,'change_product'),(48,'Can delete 产品',16,'delete_product'),(49,'Can add 文件下载',17,'add_filedown'),(50,'Can change 文件下载',17,'change_filedown'),(51,'Can delete 文件下载',17,'delete_filedown'),(52,'Can add 留言板',18,'add_leavemessage'),(53,'Can change 留言板',18,'change_leavemessage'),(54,'Can delete 留言板',18,'delete_leavemessage'),(55,'Can add 子标题',19,'add_subtitle'),(56,'Can change 子标题',19,'change_subtitle'),(57,'Can delete 子标题',19,'delete_subtitle'),(58,'Can add 公司荣誉',20,'add_honor'),(59,'Can change 公司荣誉',20,'change_honor'),(60,'Can delete 公司荣誉',20,'delete_honor'),(61,'Can add 网站配置',21,'add_sysconfig'),(62,'Can change 网站配置',21,'change_sysconfig'),(63,'Can delete 网站配置',21,'delete_sysconfig'),(64,'Can add 微信图片设置',22,'add_wximage'),(65,'Can change 微信图片设置',22,'change_wximage'),(66,'Can delete 微信图片设置',22,'delete_wximage'),(67,'Can add 首页图文配置',23,'add_companyimage'),(68,'Can change 首页图文配置',23,'change_companyimage'),(69,'Can delete 首页图文配置',23,'delete_companyimage'),(70,'Can add 合作案例',24,'add_case'),(71,'Can change 合作案例',24,'change_case'),(72,'Can delete 合作案例',24,'delete_case');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$24000$2W4NSIEeNBIT$5hzpOCF/hvsVU07uVoH6etxPFNrUNBVD2ZD7c8+gbEM=','2016-05-19 14:54:44.410724',1,'admin','','','123@123.com',1,1,'2016-05-09 12:26:52.162897');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2016-05-09 12:33:08.714014','1','/',1,'Added.',9,1),(2,'2016-05-09 12:33:12.872893','1','首页',1,'Added.',10,1),(3,'2016-05-09 12:34:55.368035','2','/comon/?slug=message',1,'Added.',9,1),(4,'2016-05-09 12:35:09.793765','2','/common/?slug=message',2,'已修改 name 。',9,1),(5,'2016-05-09 12:36:04.471231','2','实力',1,'Added.',10,1),(6,'2016-05-09 12:36:41.510919','3','/common/?slug=new',1,'Added.',9,1),(7,'2016-05-09 12:36:45.391483','3','新闻',1,'Added.',10,1),(8,'2016-05-09 12:37:26.128201','4','/comon/?slug=project',1,'Added.',9,1),(9,'2016-05-09 12:37:29.367451','4','产品',1,'Added.',10,1),(10,'2016-05-09 12:38:17.960104','5','/common/?slug=case',1,'Added.',9,1),(11,'2016-05-09 12:38:23.032944','5','案例',1,'Added.',10,1),(12,'2016-05-09 12:38:52.569311','6','/common/?slug=job',1,'Added.',9,1),(13,'2016-05-09 12:38:58.095803','6','招聘',1,'Added.',10,1),(14,'2016-05-09 12:39:41.082289','7','/common/?slug=contact',1,'Added.',9,1),(15,'2016-05-09 12:39:48.256159','7','联系',1,'Added.',10,1),(16,'2016-05-09 12:40:30.222820','1','img/carousel/index-banner2.jpg',1,'Added.',12,1),(17,'2016-05-09 12:40:41.585370','2','img/carousel/index-banner3.jpg',1,'Added.',12,1),(18,'2016-05-09 12:40:46.474989','3','img/carousel/index-banner3_Moobv7o.jpg',1,'Added.',12,1),(19,'2016-05-09 12:40:52.801485','4','img/carousel/index-banner4.jpg',1,'Added.',12,1),(20,'2016-05-09 12:42:54.107105','1','ECOSYSY',1,'Added.',21,1),(21,'2016-05-09 12:46:38.535938','7','联系',2,'已修改 url 和 name 。',9,1),(22,'2016-05-09 12:46:45.488015','6','招聘',2,'已修改 url 和 name 。',9,1),(23,'2016-05-09 12:46:54.098184','5','案例',2,'已修改 url 和 name 。',9,1),(24,'2016-05-09 12:47:01.848372','4','产品',2,'已修改 url 和 name 。',9,1),(25,'2016-05-09 12:47:08.575062','3','新闻',2,'已修改 url 和 name 。',9,1),(26,'2016-05-09 12:47:16.759097','2','实力',2,'已修改 url 和 name 。',9,1),(27,'2016-05-09 12:47:28.905002','1','首页',2,'已修改 url 和 name 。',9,1),(28,'2016-05-10 02:27:14.890564','1','公司简介',1,'Added.',19,1),(29,'2016-05-10 02:27:42.928796','2','发展历程',1,'Added.',19,1),(30,'2016-05-10 02:28:15.072044','3','董事长致辞',1,'Added.',19,1),(31,'2016-05-10 02:28:41.793808','4','企业文化',1,'Added.',19,1),(32,'2016-05-10 02:28:47.647416','1','公司简介',2,'已修改 top 。',19,1),(33,'2016-05-10 02:33:47.784126','7','联系',2,'已修改 url 。',9,1),(34,'2016-05-10 02:33:52.491524','6','招聘',2,'已修改 url 。',9,1),(35,'2016-05-10 02:33:56.770458','5','案例',2,'已修改 url 。',9,1),(36,'2016-05-10 02:34:01.025850','4','产品',2,'已修改 url 。',9,1),(37,'2016-05-10 02:35:05.401024','7','联系',2,'已修改 url 。',9,1),(38,'2016-05-10 02:35:13.620422','6','招聘',2,'已修改 url 。',9,1),(39,'2016-05-10 02:35:44.671024','5','案例',2,'已修改 url 。',9,1),(40,'2016-05-10 02:35:53.083663','4','产品',2,'已修改 url 。',9,1),(41,'2016-05-10 02:36:00.479139','3','新闻',2,'已修改 url 。',9,1),(42,'2016-05-10 02:36:09.384271','2','实力',2,'已修改 url 。',9,1),(43,'2016-05-10 02:38:00.310263','3','新闻',2,'已修改 url 。',9,1),(44,'2016-05-10 02:38:04.551918','7','联系',2,'已修改 url 。',9,1),(45,'2016-05-10 02:38:08.558551','6','招聘',2,'已修改 url 。',9,1),(46,'2016-05-10 02:38:13.533146','5','案例',2,'已修改 url 。',9,1),(47,'2016-05-10 02:38:21.206034','4','产品',2,'已修改 url 。',9,1),(48,'2016-05-10 02:38:32.032635','2','实力',2,'已修改 url 。',9,1),(49,'2016-05-10 02:48:27.630636','1','实力',1,'Added.',11,1),(50,'2016-05-10 05:31:20.980365','5','人才理念',1,'Added.',19,1),(51,'2016-05-10 05:32:02.791412','8','服务',1,'Added.',9,1),(52,'2016-05-10 05:32:06.045017','8','服务',1,'Added.',10,1),(53,'2016-05-10 05:41:41.548754','8','服务',3,'',10,1),(54,'2016-05-10 06:00:43.091296','3','新闻',2,'已修改 url 。',9,1),(55,'2016-05-10 06:00:52.565918','3','新闻',2,'没有字段被修改。',10,1),(56,'2016-05-10 06:01:11.458478','4','产品',2,'已修改 url 。',9,1),(57,'2016-05-10 06:01:12.921381','4','产品',2,'没有字段被修改。',10,1),(58,'2016-05-10 06:07:32.557013','1','新闻资讯',1,'Added.',7,1),(59,'2016-05-10 06:07:46.779762','1','摩力顿润滑油2015全国巡回技术交流会朔同站成功召开',1,'Added.',13,1),(60,'2016-05-10 06:21:54.920752','2','新闻',1,'Added.',11,1),(61,'2016-05-10 06:22:09.078900','2','新闻',2,'已修改 img 。',11,1),(62,'2016-05-10 06:22:52.263572','2','新闻',2,'已修改 img 。',11,1),(63,'2016-05-10 07:54:58.795915','2','齿轮油如何选择，齿轮油选择注意事项',1,'Added.',13,1),(64,'2016-05-10 07:55:56.790373','3','摩力顿纳米修复剂缓解机械密封磨损',1,'Added.',13,1),(65,'2016-05-10 12:46:41.071826','1','车辆润滑油',1,'Added.',15,1),(66,'2016-05-10 12:47:37.097223','1','M6抗磨液压油 L-HM 4L',1,'Added.',16,1),(67,'2016-05-10 12:48:01.437715','2','摩托车机油',1,'Added.',15,1),(68,'2016-05-10 12:48:35.751368','2','M6活性摩托车机油 900ml',1,'Added.',16,1),(69,'2016-05-10 12:51:27.241807','3','产品',1,'Added.',11,1),(70,'2016-05-11 01:31:16.727329','3','bbbb',1,'Added.',16,1),(71,'2016-05-11 01:31:33.909392','4','xxxxx',1,'Added.',16,1),(72,'2016-05-11 01:47:02.926840','5','mmmm',1,'Added.',16,1),(73,'2016-05-11 02:19:08.800731','7','联系',2,'已修改 url 。',9,1),(74,'2016-05-11 02:20:18.729777','7','联系',2,'已修改 url 。',9,1),(75,'2016-05-11 02:22:26.088451','4','联系',1,'Added.',11,1),(76,'2016-05-11 04:40:34.275355','5','招聘',1,'Added.',11,1),(77,'2016-05-11 06:01:31.109360','9','index',1,'Added.',10,1),(78,'2016-05-12 05:40:08.907336','2','企业文化',1,'Added.',7,1),(79,'2016-05-12 05:40:12.058905','4','企业文化',2,'已修改 img 。',19,1),(80,'2016-05-12 12:52:55.374774','1','公司简介',2,'已修改 introduce 。',19,1),(81,'2016-05-12 12:53:06.109729','5','人才理念',2,'已修改 introduce 。',19,1),(82,'2016-05-12 12:53:19.236158','4','企业文化',2,'已修改 introduce 。',19,1),(83,'2016-05-12 12:53:30.488035','3','董事长致辞',2,'已修改 introduce 。',19,1),(84,'2016-05-12 12:53:45.498579','2','发展历程',2,'已修改 introduce 。',19,1),(85,'2016-05-12 13:01:44.731372','3','人才理念',1,'Added.',7,1),(86,'2016-05-12 13:01:47.262331','5','人才理念',2,'已修改 img 。',19,1),(87,'2016-05-12 13:02:12.162987','4','董事长致辞',1,'Added.',7,1),(88,'2016-05-12 13:02:21.354382','3','董事长致辞',2,'已修改 img 。',19,1),(89,'2016-05-12 13:02:45.796653','5','发展历程',1,'Added.',7,1),(90,'2016-05-12 13:02:47.624688','2','发展历程',2,'已修改 img 。',19,1),(91,'2016-05-12 13:03:28.485228','6','公司简介',1,'Added.',7,1),(92,'2016-05-12 13:03:30.023549','1','公司简介',2,'已修改 img 。',19,1),(93,'2016-05-12 13:07:55.818634','3','董事长致辞',2,'已修改 introduce 。',19,1),(94,'2016-05-12 13:33:03.882064','3','摩力顿纳米修复剂缓解机械密封磨损',2,'已修改 img 。',13,1),(95,'2016-05-12 13:33:09.969262','2','齿轮油如何选择，齿轮油选择注意事项',2,'没有字段被修改。',13,1),(96,'2016-05-12 13:48:28.001317','4','工业润滑油检验方法 现在润滑油快速检验方法',1,'Added.',13,1),(97,'2016-05-12 13:49:02.717188','5','润滑油添加剂有什么作用',1,'Added.',13,1),(98,'2016-05-12 13:49:35.754948','6','工业润滑油检验方法 现在润滑油快速检验方法',1,'Added.',13,1),(99,'2016-05-12 13:50:45.283360','7','纳米润滑油如何使用 纳米润滑油添加方法',1,'Added.',13,1),(100,'2016-05-12 14:29:16.162426','7','纳米润滑油如何使用 纳米润滑油添加方法',2,'已修改 content 。',13,1),(101,'2016-05-12 14:29:23.692156','6','工业润滑油检验方法 现在润滑油快速检验方法',2,'已修改 content 。',13,1),(102,'2016-05-12 14:29:29.192929','5','润滑油添加剂有什么作用',2,'没有字段被修改。',13,1),(103,'2016-05-12 14:29:39.646140','4','工业润滑油检验方法 现在润滑油快速检验方法',2,'已修改 content 。',13,1),(104,'2016-05-13 01:46:37.717859','1','国通金大',2,'已修改 logo 和 title 。',21,1),(105,'2016-05-16 12:28:57.272180','1','引领行业创新发展十大功勋企业家',1,'Added.',20,1),(106,'2016-05-16 12:29:13.142825','2','江苏省民营科技企业',1,'Added.',20,1),(107,'2016-05-16 12:30:00.494727','3','发明',1,'Added.',20,1),(108,'2016-05-16 12:30:09.948122','4','信息',1,'Added.',20,1),(109,'2016-05-16 12:30:20.892007','5','好处',1,'Added.',20,1),(110,'2016-05-16 12:30:33.954591','6','产品',1,'Added.',20,1),(111,'2016-05-16 13:06:25.138775','1','企业历史',1,'Added.',23,1),(112,'2016-05-16 13:07:01.633644','2','企业简介',1,'Added.',23,1),(113,'2016-05-16 13:07:17.123503','3','企业核心',1,'Added.',23,1),(114,'2016-05-16 14:22:23.410330','1','baidu公司',1,'Added.',24,1),(115,'2016-05-16 14:28:15.747640','1','baidu公司',2,'已修改 url 。',24,1),(116,'2016-05-16 14:29:39.847910','6','案例',1,'Added.',11,1),(117,'2016-05-17 13:59:58.878518','1','国金通大',2,'已修改 title 。',21,1),(118,'2016-05-18 06:57:42.617818','1','国金通大',2,'已修改 record, public 和 subscribe 。',21,1),(119,'2016-05-18 07:05:00.938970','1','国金通大',2,'已修改 telephone 和 qq 。',21,1),(120,'2016-05-19 14:57:01.681570','1','国金通大',2,'已修改 logo 。',21,1),(121,'2016-05-19 14:59:47.247072','1','国金通大',2,'已修改 logo 。',21,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(12,'apps','carousel'),(24,'apps','case'),(23,'apps','companyimage'),(8,'apps','file'),(17,'apps','filedown'),(20,'apps','honor'),(7,'apps','images'),(14,'apps','jobs'),(18,'apps','leavemessage'),(10,'apps','nav'),(11,'apps','navimages'),(9,'apps','navurls'),(13,'apps','news'),(16,'apps','product'),(15,'apps','producttypes'),(19,'apps','subtitle'),(21,'apps','sysconfig'),(22,'apps','wximage'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-05-09 12:25:11.755100'),(2,'auth','0001_initial','2016-05-09 12:25:20.587107'),(3,'admin','0001_initial','2016-05-09 12:25:22.512406'),(4,'admin','0002_logentry_remove_auto_add','2016-05-09 12:25:22.669030'),(5,'apps','0001_initial','2016-05-09 12:25:37.246279'),(6,'contenttypes','0002_remove_content_type_name','2016-05-09 12:25:38.365668'),(7,'auth','0002_alter_permission_name_max_length','2016-05-09 12:25:39.125881'),(8,'auth','0003_alter_user_email_max_length','2016-05-09 12:25:39.943199'),(9,'auth','0004_alter_user_username_opts','2016-05-09 12:25:39.994187'),(10,'auth','0005_alter_user_last_login_null','2016-05-09 12:25:40.579443'),(11,'auth','0006_require_contenttypes_0002','2016-05-09 12:25:40.613497'),(12,'auth','0007_alter_validators_add_error_messages','2016-05-09 12:25:40.664196'),(13,'sessions','0001_initial','2016-05-09 12:25:41.229214'),(14,'apps','0002_remove_leavemessage_language_id','2016-05-12 01:49:12.413108'),(15,'apps','0003_subtitle_img','2016-05-12 05:36:17.910150'),(16,'apps','0004_subtitle_introduce','2016-05-12 12:51:55.774015'),(17,'apps','0005_auto_20160516_1948','2016-05-16 11:48:54.484423'),(18,'apps','0006_auto_20160516_2028','2016-05-16 12:28:29.001785'),(19,'apps','0007_auto_20160516_2211','2016-05-16 14:11:10.820380'),(20,'apps','0008_auto_20160516_2226','2016-05-16 14:26:22.905598'),(21,'apps','0009_auto_20160518_1456','2016-05-18 06:56:40.266411'),(22,'apps','0010_sysconfig_qq','2016-05-18 06:58:54.790854'),(23,'apps','0011_sysconfig_telephone','2016-05-18 07:04:43.456631'),(24,'apps','0012_auto_20160518_1537','2016-05-18 07:37:34.626004');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('44vtpdnq3dmv6wdnz41q3dy53y1tfxv1','MTJjZjc1NWFmMThmYjYxZDQ4YjdiY2I1ZTk3ODFhYmI0MjgxZmM3Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxOWNiMjk0ZWZiZGQxNWE2ZGIxMDNhNjMyYzljN2FkMTczYjVjNWJmIn0=','2016-05-30 13:23:36.007956'),('5bq8988xryswrmc9vufplb1yv397862c','YTM3MDAwNzQyMWY3ZGE2NjMxMjJkZDNhMDY1ZjQyNmEwOGUwZmNlNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjE5Y2IyOTRlZmJkZDE1YTZkYjEwM2E2MzJjOWM3YWQxNzNiNWM1YmYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-06-01 06:57:01.102679'),('8gy2hrb74q580vzh7jofpy7rju3qwjzc','MTk0OWM2NzhhMjM4YjNjYjFiZTA1NjkxMWQwZGNmYjNkOTQ2YWM3Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTljYjI5NGVmYmRkMTVhNmRiMTAzYTYzMmM5YzdhZDE3M2I1YzViZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-05-24 05:30:19.400647'),('ccis6djyz9tpq9ofghw5n9ql894xlssl','MjU3ZmVkYjk2NjBkZGIyYTUzMmRmOThhYmZlYjgwZTUzODEyMmI0Yjp7Il9hdXRoX3VzZXJfaGFzaCI6IjE5Y2IyOTRlZmJkZDE1YTZkYjEwM2E2MzJjOWM3YWQxNzNiNWM1YmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-06-01 09:05:53.491210'),('ddoll42khgj9nh3qrkav9jddnq8yucnc','ZGEwZGY0MzU2YTBlOWU2YTEwNTYyZGY2MDY2MmQ0N2E1NGUzMTUxNTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTljYjI5NGVmYmRkMTVhNmRiMTAzYTYzMmM5YzdhZDE3M2I1YzViZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-06-02 14:54:44.420872'),('eqb9gnnwfdkrxs4vrkr9u0dxlrpajvsd','MTk0OWM2NzhhMjM4YjNjYjFiZTA1NjkxMWQwZGNmYjNkOTQ2YWM3Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTljYjI5NGVmYmRkMTVhNmRiMTAzYTYzMmM5YzdhZDE3M2I1YzViZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-05-28 09:43:02.777201'),('f3uslqisoghrzwmklu97kmk3m8x85izi','MTJjZjc1NWFmMThmYjYxZDQ4YjdiY2I1ZTk3ODFhYmI0MjgxZmM3Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxOWNiMjk0ZWZiZGQxNWE2ZGIxMDNhNjMyYzljN2FkMTczYjVjNWJmIn0=','2016-05-25 01:30:43.888473'),('nb31ve6d14t3pk8ueva6oz38rjmtggf5','MTJjZjc1NWFmMThmYjYxZDQ4YjdiY2I1ZTk3ODFhYmI0MjgxZmM3Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxOWNiMjk0ZWZiZGQxNWE2ZGIxMDNhNjMyYzljN2FkMTczYjVjNWJmIn0=','2016-05-24 02:24:40.952959'),('xvfk726naap950z2ak35flefn6nh0y4d','NzU2NjU1Zjg2Y2M3ZDRmMWVlMzcwZmY0YjE3YzA0YmRhMTg1MzkzYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIxOWNiMjk0ZWZiZGQxNWE2ZGIxMDNhNjMyYzljN2FkMTczYjVjNWJmIn0=','2016-05-27 01:44:31.517777'),('xzk92kxg7qdhdm3m2k59ozo29hxqn4ff','YTM3MDAwNzQyMWY3ZGE2NjMxMjJkZDNhMDY1ZjQyNmEwOGUwZmNlNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjE5Y2IyOTRlZmJkZDE1YTZkYjEwM2E2MzJjOWM3YWQxNzNiNWM1YmYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-05-26 01:33:04.831982'),('yur53ijlvbb9p5cwvkn6cpoho5xjdzoo','MTk0OWM2NzhhMjM4YjNjYjFiZTA1NjkxMWQwZGNmYjNkOTQ2YWM3Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMTljYjI5NGVmYmRkMTVhNmRiMTAzYTYzMmM5YzdhZDE3M2I1YzViZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-05-23 12:26:56.021607');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-19 23:00:34
